       IDENTIFICATION DIVISION.
       PROGRAM-ID.  DoCalc.
       AUTHOR. Supakit Kongkam.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 FirstNum PIC 9 VALUE ZEROES.
       01 SecondNum PIC 9 VALUE ZEROES.
       01 CalcResult PIC 9 VALUE 0.
       01 UserPrompt PIC X(38) VALUE 
           "Please enter two single digit numbers".
       PROCEDURE DIVISION.
       CalculateResult.
           DISPLAY UserPrompt.
           DISPLAY "Enter a FirstNum : "
           ACCEPT FirstNum.
           DISPLAY "Enter a SecondNum : "
           ACCEPT SecondNum.
           COMPUTE CalcResult = FirstNum + SecondNum.
           DISPLAY "Result is = ", CalcResult.
           STOP RUN.



       